---
Title: Logboek   
Subtitle: Einde periode   
Date: 2018-06-04   
Tags: ["Blog", "Einde periode", "Periode4"]   
---   
    
### Zelfontwikkeling in periode 4    
*Zoals elke periode was die weer voorbij voor ik het door had. Omdat het de tweede periode was dat we met hetzelfde groepje zaten konden we elkaar beter aanvoelen en ging de samenwerking echt heel goed. Deze periode had ik de taak als teamcaptain wat ik erg interessant vond. Ik heb geprobeerd me wat meer op de achtergrond te houden omdat ik uit het groepje de meeste competenties heb gehaald. Zo geef ik de rest van het groepje ook de kans die competenties te behalen. Doordat we een goed overzicht hadden gemaakt wie welke competenties nog moest behalen konden we op een makkelijke manier de taken verdelen. Zo kan ieder zich storten op de competenties waar we ons nog in moeten bewijzen. Ik heb me de afgelopen periode gestort op professionaliseren, onderzoeken en evalueren van ontwerpresultaten. Hier heb ik erg mijn best in gedaan en ben naar mijn mening erg hierop vooruit gegaan, op naar volgend jaar!*
