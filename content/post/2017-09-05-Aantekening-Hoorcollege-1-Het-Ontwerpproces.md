---
Title: Aantekeningen    
Subtitle: Hoorcollege 1.1 Het ontwerpproces   
Date: 2017-09-05   
Tags: ["Aantekeningen", "Het ontwerpproces", "Hoorcollege", "Periode1"]   
---   
   
### Hoorcollege 1.1 Het ontwerpproces   
    
<u>Wat is design?</u>  
*“Design is everything artificial, man-made things and systems.”*   
*“Design (beroepspraktijk) is to design (actie/creatie) a design (concept/idee) to produce a design (resultaat/ding).”*   
Design is not art   
<u>Kunst</u>: Doelstelling is subjectief / intern. Gemaakt voor kijkers.
<u>Design</u>: doelstelling is objectief / extern gemaakt voor gebruikers.
*“Design is an ability.”* (vaardigheid / competentie).   
Het vermogen om een handeling bekwaam uit te voeren... wordt veelal vergaard door ervaring.   
*“Design is the ability to imagine that-which-does-not-yet-exist, to make it appear in concrete form as a new, purposeful addi6on to the real world.”*   
*“We all design when we plan for something new to happen”* =  change (verandering)   
*“Everyone designs who devises courses of action aimed at changing existing situations into preferred ones.”*   
     
<u>Levels of design</u>   
- Visionary & master  Beroepspraktijk; uitbreiden, vernieuwen; regels, context, life.   
- Expert & Proficient & Competent  Context begrijpen; regels toepassen, aanpassen, herkennen van uitzonderingen; wanneer (niet) en waarom.   
- Advanced beginner & novice  Regels leren; herkennen en verkennen; wat en hoe)   
    
*"Design is a process"* (verloop van een geleidelijke verandering beschouwd in stappen of volgorde).
Input --> process --> output   
Huidige situatie --> process --> gewenste situatie   
Ontwerpproces CMD: Ontwerp vragen --> <-- Ontwerp resultaten; Onderzoek --> <-- Concept --> <-- Prototype.    
    
<u>Stanford</u> :    
1.	Empathize: Develop a deep understanding of the challenge.    
2.	Define: Clearly articilate the problem you want to solve.    
3.	Ideate: Brainstorm potential solutions. Select and develop your solution.     
4.	Prototype: Design a protoype (or series of prototypes) to test all or part of your solution.    
5.	Test: Engage in a continuous shortcycle innovation process to continually improve your design.    
    
<u>Ideo</u> :
The five phases of the design process:
1.	Discovery;
2.	Interpretation;
3.	Ideation;
4.	Experimentation;
5.	Evolution
     
*“Design is one of the most complex ways of problem solving.”*    
    
Wicked problems: Onduidelijk & onvoorspelbaar.    
*“Design is to apply directed crea6vity to solve wicked problems.”*    
*“By trying to understand the problem and experimenting with possible solutions designing becomes a learning process.”*    