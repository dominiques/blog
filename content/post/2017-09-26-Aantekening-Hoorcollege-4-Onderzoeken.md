---
Title: Aantekeningen    
Subtitle: Hoorcollege 1.4 Onderzoeken    
Date: 2017-09-26     
Tags: ["Aantekeningen", "Onderzoeken", "Hoorcollege", "Periode1"]      
---
   
### Hoorcollege 1.4 Onderzoeken   
<u>Onderzoek in het ontwerpproces (bij CMD)</u>   
1.	Onderzoek van anderen gebruiken    
-	Studenten kunnen onderzoek, vanuit verschillende vakgebieden, vinden en waarderen en op waarde scha2en in de context van het ontwerp en het ontwerpvak.    
-	Studenten hebben zicht op de posi8e van ontwerponderzoek ten opzichte van andere vormen van onderzoek.    
2.	Onderzoek zelf doen     
-	Studenten beheersen een palet van onderzoeksmethoden en technieken en kunnen hier een verantwoorde keuze uit maken.    
-	Bij het uitvoeren van een onderzoek kunnen studenten de onderzoekscyclus doorlopen. Hierbij tonen ze kennis van de spelregels van onderzoek doen (methodologie, ethiek van onderzoek) afhankelijk van de complexiteit en noviteit van de ontwerpopgave.     
-	Studenten kunnen resultaten van onderzoek interpreteren en de kwaliteit van deze resultaten (betrouwbaarheid, validiteit, bruikbaarheid voor het ontwerp) en de beperkingen van het uitgevoerde onderzoek beoordelen.    
3.	Onderzoekende houding    
-	Nieuwsgierig;    
-	Pro-actief;    
-	Open;     
-	Kritisch;           
-	Gericht op inzicht;             
-	Bereid tot perspec8efwisseling;    
-	Bereid om oordeel uit te stellen;    
-	Routines kritisch bevragen;    
-	Kennis waarderen;    
-	Inzicht willen delen.    
     
<u>Onderzoeken</u>    
Wat is (geen) onderzoek?   
Opzoeken: vraag  informatie raadplegen  antwoord.
Uitzoeken: vraag  informatie zoeken  informatie raadplegen  informatie analyseren  antwoord.
Onderzoeken: vraag  methode bepalen  data verzamelen  data analyseren  concluderen (informatie)  antwoord  vastleggen.
Ontwerponderzoek - Science – Humanities.    
Ontwerponderzoek houding – kennis & vaardigheden.     
     
<u>Denkniveaus</u>    
-	Abstract    
-	Reflective    
-	Concreet    
-	Practice     
     
-	<u>Ontwerpresultaten</u> : Reflecteert op de waarde en maatschappelijke relevantie van het eindresultaat voor de gebruiker, opdrachtgever, andere belanghebbenden en het vakgebied en op de mate van innovatie.    
-	<u>Ontwerpproces</u> : Reflecteert op (1) de waarde van vergaarde kennis (vak-, domein- en proceskennis), (2) op de inrichting van het ontwerpproces en (3) op de invloed op en van de veranderende context.     
-	<u>Leren</u>: Reflecteert zelfstandig op (1) de eigen ontwikkeling als ontwerper, (2) op het eigen leerproces en (3) geeft toekoms8ge ontwikkelpunten aan.    
     
<u>Naar binnen gericht</u>: Art (veranderd de geest) & science (veranderd de moleculen).      
<u>Naar buiten gericht</u>: Design (veranderd de geest) & Engineering (veranderd de moleculen.    
     
<u>Ontwerpopgaven zijn vaan wicked problems</u>:    
1.	There is no definitive formulation of a wicked problem.   
2.	Wicked problems have no stopping rule.    
3.	Solutions to wicked problems are not true-or-false, but good-or-bad.         
4.	There is no immediate and no ultimate test of a solution to a wicked problem.     
5.	Every solution to a wicked problem is a "one-shot operation"; because there is no opportunity to learn by trial-and-error, every attempt counts significantly.     
6.	Wicked problems do not have an enumerable (or an exhaustively describable) set of potential solutions, nor is there a well-described set of permissible operations that may be incorporated into the plan.    
7.	Every wicked problem is essentially unique.     
8.	Every wicked problem can be considered to be a symptom of another problem.     
9.	The existence of a discrepancy representing a wicked problem can be explained in numerous ways. The choice of explanation determines the nature of the problem’s resolution.    
10.	The planner has no right to be wrong.      
     
<u>Vastleggen</u>    
Resultaten van onderzoek moet je vastleggen om aan anderen te kunnen overdragen (bv. opdrachtgever). Dit kan in de vorm van bijvoorbeeld een rapport, scriptie, proefschrift of documentaire, tentoonstelling, maar ook ontwerpverslag, of andere vormen.     
Wat leg je vast? opgave  vragen  activiteiten  resultaat  beslissing.      
      
Bij overdragen: toon rijkheid/variatie (niet de reductie). Overtuig en laat beleven. Niet alleen tekst en beeld telt, maar ook het gevoel dat je weet op te roepen.     
En bij alles ...hou het leuk! Voor jezelf, eindgebruiker, en opdrachtgever!     
