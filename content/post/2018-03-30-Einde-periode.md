---
Title: Logboek   
Subtitle: Einde periode   
Date: 2018-03-30   
Tags: ["Blog", "Einde periode", "Periode3"]   
---   
    
### Zelfontwikkeling in periode 3    
*Deze periode is echt voorbij gevlogen! Ik heb deze periode veel workshops gevolgd. Deze waren erg leerzaam! Hier heb ik veel van geleerd en heb ik goed kunnen gebruiken voor mijn competenties en deliverables. De samenwerking met dit groepje ging veel beter dan de vorige periode. De vorige periode maar iets aan één ander teamlid gehad en deze periode hadden we echt een groepje waarbij iedereen altijd aanwezig was en iedereen mee deed met alles. Ik heb dankzij de workshop creatieve technieken een creatieve sessie moeten leiden, dit vond ik erg leerzaam en ging dan ook erg goed. Ik heb hierdoor kennis gemaakt met veel creatieve methodes. Ik heb deze periode het keuzevak photoshop gevolgd waar ik me heel graag in wilde verbeteren. Dit lukt erg goed! Ik vraag veel feedback tijdens de lessen waar ik veel aan heb.*     