---
Title: Logboek   
Subtitle: Einde periode   
Date: 2018-01-19   
Tags: ["Blog", "Einde periode", "Periode2"]   
---   
    
### Zelfontwikkeling in periode 2    
*Ik heb deze periode erg veel geleerd! Aangezien ik in een groepje zat waarvan er één gewoon niet meer kwam opdagen en niets liet weten en de andere twee vrij weinig initiatief hadden en dus ook niet heel veel deden, heb ik samen met Beau eigenlijk het hele project gemaakt. Hierdoor heb ik elk beroepsproduct leren kennen en gemaakt. Ook is bijna alles gevalideerd wat betekend dat we/ik er ook erg veel van heb geleerd. Bij het begin kende ik de meeste termen nog geen eens, en nu heb ik ze allemaal leren kennen. Hier ben ik erg blij om. De vorige periode kreeg ik als peerfeedback wel een aantal dingen waar ik op moest letten. Ik heb er heel goed op gelet en er is geen enkel groepslid die nu dezelfde feedback aan mij heeft gegeven. Dit betekend dat ik zeker vooruit ben gegaan. Ik heb veel voor mijn groepje kunnen betekenen waar ik erg blij mee ben! Ook heb ik deze periode twee keuzevakken gevolgd: InDesign en infographics. Ik zal hier binnenkort het eindproduct voor moeten inleveren waardoor ik kan zien of ik daar ook genoeg van heb geleerd. Naar mijn mening ben erg verbeterd in InDesign en heb ook geleerd wat een infographic is en hoe het te werk gaat. Het is een geslaagde periode die ik goed afgesloten achter me kan laten!*
