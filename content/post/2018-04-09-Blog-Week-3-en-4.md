---
Title: Logboek   
Subtitle: Blog week 3 en 4   
Date: 2018-04-23   
Tags: ["Blog", "To do", "Done", "Periode4"]   
---   
### Week 3     
   
<u>Maandag 23 April 2018</u>    
*Vandaag was ik een dagje ziek thuis, mijn groepje heeft interview vragen verder voorbereid op basis van de observatie en ze zijn ze naar Afrikaanderwijk geweest om te interviewen.*      

<u>Dinsdag 24 April 2018</u>     
*Vandaag was het eerste hoorcollege waar ik naartoe ben gegaan.*     
        
<u>Woensdag 25 April 2018</u>    
*Vandaag zijn we weer gaan interviewen, we hebben niet veel mensen kunnen vinden jammer genoeg.*     
    
<u>Donderdag 26 April 2018</u>    
X     
     
<u>Vrijdag 27 April 2018</u>    
X      
     
<u>Zaterdag 28 April 2018</u>    
X      
     
<u>Zondag 29 April 2018</u>    
X     
     

### Meivakantie

### Week 4    
    
<u>Maandag 07 Mei 2018</u>    
*Vandaag hebben we peerfeedback gedaan met ons groepje en de interviewvragen aangepast.*     

<u>Dinsdag 08 Mei 2018</u>     
*Vandaag was het tweede hoorcollege waar ik heen ging.*      
     
<u>Woensdag 09 Mei 2018</u>    
*Vandaag zijn we naar Afrikaanderwijk geweest en zijn we naar inholland geweest en nog naar middelbare scholen die dicht waren wegens vakantie.*    
    
<u>Donderdag 10 Mei 2018</u>    
X    

<u>Vrijdag 11 Mei 2018</u>    
X     

<u>Zaterdag 12 Mei 2018</u>    
X     
    
<u>Zondag 13 Mei 2018</u>    
X     
