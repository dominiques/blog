---
Title: Logboek   
Subtitle: Blog week 5, 6 en 7  
Date: 2018-05-14   
Tags: ["Blog", "To do", "Done", "Periode4"]   
---   
### Week 5     
   
<u>Maandag 14 Mei 2018</u>    
*Vandaag hebben we een creatieve sessie gedaan onder leiding van Demi, hieruit hebben we ons concept bedacht, het komt neer op een foodtruck festival in Afrikaanderwijk.*     
    
<u>Dinsdag 15 Mei 2018</u>     
*Vandaag was het derde hoorcollege waar ik naartoe ben gegaan.*     
        
<u>Woensdag 16 Mei 2018</u>    
*Vandaag zijn we het concept aan het uitdenken.*     
    
<u>Donderdag 17 Mei 2018</u>    
X     
     
<u>Vrijdag 18 Mei 2018</u>    
X      
     
<u>Zaterdag 19 Mei 2018</u>    
X      
     
<u>Zondag 20 Mei 2018</u>    
X     
     

### Week 6    
    
<u>Maandag 21 Mei 2018</u>    
*Vandaag was het pinksteren.*    

<u>Dinsdag 22 Mei 2018</u>     
*Vandaag was ik onderweg naar het hoorcollege en toen kwam ik erachter dat er helemaal geen hoorcollege was. Dit was erg slecht gecommuniceerd omdat het alleen op Facebook werd geplaatst in een groep waar ik persoonlijk niet eens in zat.*    

<u>Woensdag 23 Mei 2018</u>    
*Vandaag gingen we met het groepje aan de slag met het uitwerken van het concept. Aangezien we nog maar weinig dagen hebben zijn we van plan om wat taken te verdelen en voor ons zelf een aantal taken te maken.*   

<u>Donderdag 24 Mei 2018</u>    
*Thuis heb ik aan mijn foodtrucks gewerkt en recepten uitgezocht die gezond zijn en bij de surinaamse cultuur horen.*    

<u>Vrijdag 25 Mei 2018</u>    
X     

<u>Zaterdag 26 Mei 2018</u>    
X     
    
<u>Zondag 27 Mei 2018</u>    
X    


### Week 7     
   
<u>Maandag 28 Mei 2018</u>    
*Vandaag zijn we de foodtrucks aan elkaar laten zien zien en heb ik mijne alvast uitgeprint en uitgeknipt. Ook hebben we de plattegrond waar de foodtrucks op willen zetten afgedrukt op A1, en hebben we de poster op A2 uitgeprint.*     
    
<u>Dinsdag 29 Mei 2018</u>     
*Vandaag hadden we hoorcollege en daarna zijn we met het groepje naar Afrikaanderwijk gegaan om ons low-fid prototype te testen. Hiervoor hebben we een storyboard gebruikt. De testpersonen waren erg enthousiast.*     
        
<u>Woensdag 30 Mei 2018</u>    
*Vandaag hebben we een testrapportage gemaakt en hebben we de rest van de foodtrucks uitgeprint. Deze hebben we uitgeknipt en met een tussenbalk 3D gemaakt.*     
    
<u>Donderdag 31 Mei 2018</u>    
X     
     
<u>Vrijdag 01 Juni 2018</u>    
*Vandaag was de expo en zijn we eerder naar school gegaan om de laatste puntjes op de i te zetten. De conceptposters zijn afgemaakt en we hebben alles uitgeprint. Vervolgens de expo opgebouwd en toen begon het. Het ging erg goed en het is dan ook gevalideerd. Hier ben ik erg blij om. Er was jammer genoeg geen tijd meer om feedback van Bob te krijgen op verschillende beroepsproducten dus dit heb ik met hem overlegd en mag ik later mailen.*      
     
<u>Zaterdag 02 Juni 2018</u>    
*Vandaag heb ik aan mijn leerdossier gewerkt. Ik moet nog 3 competenties halen: professionaliseren, onderzoeken en evalueren van ontwerpresultaten dus ik doe mijn uiterste best om ook deze laatste drie af te sluiten op eind niveau leerjaar 1.*      
     
<u>Zondag 03 Juni 2018</u>    
*Vandaag heb ik mijn leerdossier afgemaakt en ingeleverd.*     
     
     
