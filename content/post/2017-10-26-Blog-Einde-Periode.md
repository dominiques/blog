---
Title: Logboek   
Subtitle: Einde periode   
Date: 2017-10-26   
Tags: ["Blog", "Einde periode", "Periode1"]   
---   
    
### Zelfontwikkeling in periode 1    
*Ik heb veel geleerd van de eerste periode. Ik ben een stuk beter geworden in samenwerken. Ook heb ik mezelf sterk verbeterd in brainstormen en onderzoeken. Voordat ik op deze opleiding begon was ik daar minder goed in. Ik heb dan ook verschillende en nieuwe manieren ontdekt om dit te doen. Ook heb ik extra oefening gehad in presenteren waar ik dan ook erg op vooruit ben gegaan. Verder ben ik heel blij dat ik het keuzevak illustrator heb gevolgd; eerst kon ik net aan een lijn trekken en nu kan ik al landschappen tekenen en avatars ontwerpen. Door de feedback die ik een aantal keer heb ontvangen heb ik mezelf verbeterd en heb ik specifiek gehoord waarin en wat ik moet verbeteren, dit heeft mij erg geholpen. Ik ben in deze eerste periode in mijn vaardigheden vooruitgegaan en heb kennis gemaakt met nieuwe programma’s en manieren van uitvoering.*   
