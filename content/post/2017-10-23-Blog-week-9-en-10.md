---
Title: Logboek   
Subtitle: Blog week 9 en 10   
Date: 2017-10-23   
Tags: ["Blog", "To do", "Done", "Periode1"]   
---   
### Week 9       
<u>Maandag 23 oktober 2017</u>    
*Vandaag hebben we de escaperoom verder gemaakt. Ik heb eerst samen met Quinty de doos zwart proberen te spuiten maar dat werkte niet dus toen heb ik een deel van de doos geverft. Vervolgens hebben we met ze alle de ontwerpproceskaart gemaakt. Ook heb ik boeken en een koffertje gevouwen voor in de escaperoom.Thuis heb ik mijn minigames verder uitgewerkt en een naam en logo bedacht: Travelers, en als logo een klok en dan in plaats van de cijfers, de letters travelers.*      
    
<u>Dinsdag 24 oktober 2017</u>     
*Vandaag ben ik eigenlijk vrij maar hebben we besloten nog wel met het groepje naar school te gaan om alles af te maken voor de presentatie van morgen. We hebben de escaperoom helemaal afgemaakt en alle meubels etc erin geplakt. Ook heb ik plekken voor de minigames bedacht en op de kaart aangekruisd en heb ik mijn 2 minigames uitgetest en geanalyseerd door een ander groepje. Vervolgens hebben we de spelverloop en de spelregels nog een keer met ze alle doorgenomen en nu hebben we alles klaar voor morgen.*    

<u>Woensdag 25 oktober 2017</u>    
*Vandaag was de expo. We hadden een uur om alles klaar te zetten en toen heb ik het logo nog even digitaal gemaakt want hij was alleen getekend. Vervolgens bij de expo ben ik eerst gaan rondlopen en gaan kijken wat de anderen voor games hadden bedacht. Er waren een paar erg gave ideeeën. Toen ben ik terug gegaan naar onze tafel om aan mensen die het wilde uit te leggen hoe onze game in elkaar zit en werkt. Ik heb dan ook erg leuke reacties gehoord over de game van ons groepje. Na de prijsuitreiking hebben we de spelanalyse afgemaakt en heb ik met mijn groepje de peerfeedback voor elkaar geschreven. Om 3 uur hadden we studiecoaching waar alles voor het leerdossier nog extra werd verduidelijkt. Hier hebben we het over G E V R, Gedrag, Effect, Verwachting en Resultaat gehad wat je moet gaan invullen nadat je je peerfeedback heb gekregen. Nadat alles helemaal duidelijk was voor het leerdossier mochten we naar huis. Thuis heb ik mijn illustrator huiswerk gemaakt voor morgen wat nu helemaal af is.*    

<u>Donderdag 26 oktober 2017</u>    
*Vanmorgen begon in met een les illustrator, het keuzevak wat ik volg. Toen heb ik Quinty nog even geholpen met haar blog want ze kwam er niet helemaal uit. Ook hebben we toen aan een tweedejaars student extra advies gevraagd over het leerdossier en heeft ze die van haar laten zien van vorig jaar. Vervolgens ben ik naar huis gegaan en verder gegaan aan mijn STARRT's schrijven en mijn leerdossier afmaken. Toen ik klaar was met mijn STARRT's vroeg Serhat, iemand uit mijn groepje of ik hem kon helpen met zijn blog en dat heb ik dan ook gedaan, daar was hij erg blij mee! Vervolgens heb ik mijn leerdossier nog een keer doorgelezen en de laatste foutjes eruit gehaald en heb ik hem in geleverd.*    

<u>Vrijdag 27 oktober 2017</u>    
*Vandaag was om 11:00 de deadline van het leerdossier. Aangezien ik hem gisterenavond al had ingeleverd hoefde ik vandaag niets meer te doen aan school!*    

<u>Zaterdag 28 oktober 2017</u>    
**    
    
<u>Zondag 29 oktober 2017</u>    
 **    
    
### Week 10    
     
<u>Maandag 30 oktober 2017</u>    
**     

<u>Dinsdag 31 oktober 2017</u>     
**    

<u>Woensdag 1 november 2017</u>    
**    

<u>Donderdag 2 november 2017</u>    
**    

<u>Vrijdag 3 november 2017</u>    
**   

<u>Zaterdag 4 november 2017</u>    
**    

<u>Zondag 5 november 2017</u>    
**       
    