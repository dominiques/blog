---
Title: Logboek   
Subtitle: Blog week 1 en 2   
Date: 2017-08-30   
Tags: ["Blog", "To do", "Done", "Periode1"]   
---   
### Week 1       
<u>Woensdag 30 augustus 2017</u>      
*Vandaag hebben we een groepsnaam, groepslogo bedacht. Ik zit in het groepje met Serhat, Puck, Quinty en Robin. We hebben de groepsnaam OOTB^2, wat staat voor out of the box box.  Het was een kort dagje en veel proces was er vandaag dus niet.*        
     
<u>Donderdag 31 augustus 2017</u>    
*Vandaag heb ik mijn eerste hoorcollege gehad, het ging over games. Ook hebben we hierbij gelijk de opdracht ontvangen:*    
Ontwerp een spel dat gebruik maakt van een online interactief element dat in de introductieweek van 2018 studenten in Rotterdam met elkaar verbindt.    
-	Voor: specifieke studierichting.    
-	Opdrachtgever: Hogeschool Rotterdam.    
-	Thema: typisch Rotterdam.    
 Maak een speelbaar spel met een offline en een online component.    
    
### Week 2    
     
<u>Maandag 4 september 2017</u>    
*Vandaag hebben we met het team de regels vastgesteld en een teamcaptain gekozen, dat is uiteindelijk Puck geworden. Ook hebben we de taakverdeling gemaakt. Vervolgens hebben we studiecaching gekregen. Hierna kregen we de opdracht om een teamposter te maken. Toen hadden we even wat tijd om te gaan brainstormen over de game die we willen gaan maken.*    
     
<u>Dinsdag 5 september 2017</u>     
*Vandaag heb ik een hoorcollege Design theorie gehad over het ontwerpproces. Ookal ging het allemaal erg snel heb ik het toch aardig kunnen volgen en begrijpen. Toen ik thuis kwam heb ik mijn game en de doelgroep verder onderzocht en aan mijn moodboards gewerkt.*     
    
<u>Woensdag 6 september 2017</u>    
*Vandaag startte ik een beetje lastig op, ik wist even niet meer zo goed waar ik verder mee moest gaan, maar nadat we even werden wakker geschud ben ik aan mijn moodboards verder gegaan en hebben we met het groepje een enquete gemaakt om verder te kunnen onderzoeken. Later vandaag was er een workshop blog maken waardoor ik vervolgens ben begonnen met mijn online blog.*    
    
<u>Donderdag 7 september 2017</u>    
*Vandaag had ik een kort dagje, alleen het werkcollege, dit was voor het eerst dus ik wist niet wat ik kon verwachten. We gingen de informatie van het hoorcollege verwerken in een opdracht om de informatie beter te kunnen begrijpen. Dit was erg handig, want ik dacht dat ik alles al begreep maar blijkbaar toch niet helemaal, nu is het allemaal een stuk duidelijker geworden.*    
    
<u>Vrijdag 8 september 2017</u>    
*Vandaag had ik een dagje vrij, erg lekker maar het was wel zo handig om alsnog een beetje verder te brainstormen over mijn game, dit heb ik nog een tijdje gedaan.*    
    
<u>Zaterdag 9 september 2017</u>    
*Vandaag hebben we met ons groepje bepaald welke game we gaan gebruiken en hoe we deze game verder uit gaan werken. We hebben besloten de basis van Robin en mijn game te gebruiken en vervolgens van de rest de leukste ideeën erin te verwerken. Hoe we dit precies gaan uitwerken gaan we maandag verder bespreken.*    
    
<u>Zondag 10 september 2017</u>    
 x    
    