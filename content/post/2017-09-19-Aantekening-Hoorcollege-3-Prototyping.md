---
Title: Aantekeningen    
Subtitle: Hoorcollege 1.3 Prototyping    
Date: 2017-09-19     
Tags: ["Aantekeningen", "Prototyping", "Hoorcollege", "Periode1"]   
---  

### Hoorcollege 1.3 Prototyping  
   
-	Introspection illusion (zelfreflectie illusie)   
-	Relevant: Design the right thing   
-	Passend: Design the thing right   
-	Breedte: antwoord op de vraag; divigeren   
-	Diepte: benadering van het eindproduct; converteren   
    
<u>Hoe haal je het meest uit een prototype?</u>   
-	Testplan opstellen met enkelvoudige vraag, verwachte uitkomsten, implicaties voor concept en draaiboek.   
-	Test objectief registreren.   
-	Uitkomsten interpreteren.   
-	Verassingen toelaten.   
-	Geprioriteerde lijst van actiepunten voor je concept / process.   
   
*“Failure is success in progress.”* ~ Albert Einstein   
