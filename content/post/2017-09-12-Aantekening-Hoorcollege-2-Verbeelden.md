---
Title: Aantekeningen    
Subtitle: Hoorcollege 1.2 Verbeelden    
Date: 2017-09-12     
Tags: ["Aantekeningen", "Verbeelden", "Hoorcollege", "Periode1"]   
---   
    
### Hoorcollege 1.2 Verbeelden       
-	Verbeelden: *“De ontwerper geeft vorm aan inzichten, ideeën, concepten en dergelijke en werkt deze uit tot betekenisvolle, overtuigende en coherente beelden, schema’s en prototypes”*.   
-	Met name kerntaak V&U Visueel uitwerken:    
-	*“Toepassen van verschillende theorieën en principes van visual design”*    
-	Beeld: alle communicatiemiddelen die niet primair tekst zijn, doormiddel van een tweedimensionaal medium tot ons komen, en primair een communicatief-retorische functie hebben.   
-	Communicatie: het overbrengen van een boodschap, van zender naar ontvanger.    
-	Retor = spreker   
-	Communicatief-retorische functie: overtuigen   
   
<u>Visuele communicatie</u>    
-	Leren kijken   
-	Beeldenbegrijpeneninterpreteren    
-	Beelden kiezen en maken (en daarmee overtuigen)     
-	= Visuele geletterdheid: de mogelijkheid visuele boodschappen te begrijpen en te produceren (twee perspectieven: de maker en de waarnemer).    
<u>Maken</u>    
-	Foto’s, film, posters, websites, etc.    
-	Spontaan (foto’s maken tijdens je vakantie), of bedacht.    
-	Schetsen: idee-ontwikkeling, overdragen/bespreken en bewaren     
     
<u>Waarnemen</u>    
-	Zien – Gestalt
-	Begrijpen – Semiotiek
-	Overtuigen - Visuele Retorica
-	Gestalt, semiotiek en retorica vormen samen een eenheid.
    
<u>Zien; gestalt wetten</u>   
-	Waarom zie we het ene als voorgrond en het andere als achtergrond?     
-	Hoe kan het dat we vormen kunnen onderscheiden?     
-	Wat is een goede vorm?    
-	Hoe en waarom zie we samenhang tussen dingen?    
•	Wet van voorgrond en achtergrond;    
•	Wet van symmetrie;    
•	Wet van ingevuld hiaat;    
•	Wet van overeenkomst;    
•	Wet van focuspunt;    
•	Wet van eenvoud;    
•	Wet van continuïteit;    
•	Wet van nabijheid;     
•	Wet van geslotenheid;    
•	Wet van gelijke achtergrond;    
       
<u>Begrijpen; Semiotiek</u>    
-	Iconische tekens: vertonen gelijkenisrelatie (overeenkomst).    
-	Indexicale tekens: verwijzen naar iets (deel-geheel/oorzaak-gevolg)    
-	Symbolische tekens: tekens die op afspraken, cultuur of traditie berusten.    
     
<u>Overtuigen; Retorica</u>    
-	Overtuigen (Aristoteles)    
-	Ethos - Geloofwaardigheid, eigen kwaliteiten    
-	Pathos – Emotie    
-	Logos - Argument: feiten, onderzoek, oorzaak-gevolg, vergelijkingen, praktische voordelen, etc.     
    
<u>Stijlmiddelen: versterken van beeld</u>    
Regelmatigheden (herhaling):    
-	Beeldrijm    
-	Repetitio    
-	Contrast    
-	Verbo-picturaal schema    
Onregelmatigheden (tropen)     
-	Metafoor    
-	Een deel van het geheel   
-	Vergelijking    
-	Personificatie    
-	Hyperbool    
-	Oxymoron    
-	Pastiche    
